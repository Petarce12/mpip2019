package ukim.finki.mpip2019;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.SearchView;
import retrofit2.Call;
import ukim.finki.mpip2019.adapters.CustomListAdapter;
import ukim.finki.mpip2019.asynctask.DzPlaylistAsyncTask;
import ukim.finki.mpip2019.client.DzApiClient;
import ukim.finki.mpip2019.db.DbProvider;
import ukim.finki.mpip2019.holders.CustomListViewHolder;
import ukim.finki.mpip2019.models.DzPlaylist;
import ukim.finki.mpip2019.models.DzTrack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static android.provider.AlarmClock.*;


public class Activity2 extends AppCompatActivity implements PlaylistInterface {

    private Button button2;
    List<String> dataset;
    CustomListAdapter adapter;

    Logger logger = Logger.getLogger("Activity2");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);


        initToolbar();
        initListView();
        loadData();

        DbProvider provider = new DbProvider(getApplicationContext());
        new Thread(()->{List<DzTrack> allTracks = provider.getAllTracks();}).start();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);
    }

    private void initListView() {
        RecyclerView recyclerView =  findViewById(R.id.recycler_view_1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        initDataset();

        adapter = new CustomListAdapter(dataset, getItemViewOnClickListener());
        recyclerView.setAdapter(adapter);
    }

    private void loadData() {
        DzPlaylistAsyncTask asyncTask = new DzPlaylistAsyncTask(this);
        asyncTask.execute(1867419722L);
    }

    public void loadedDzPlaylist(DzPlaylist playlist) {
        // TODO: 2019-11-06 Should load through LiveData
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.menu_item1).getActionView();

        searchView.setOnQueryTextListener(getOnQueryTextListener());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item1:
                ///
                logger.info("Clicked menu item: 1");
                break;
            case R.id.menu_item2:
                ///
                logger.info("Clicked menu item: 2");
                break;
            case R.id.menu_item3:
                //
                logger.info("Clicked menu item: 3");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void insertElementIntoList(String element) {
        dataset.add(element);
        adapter.notifyDataSetChanged();
    }

    private void initDataset() {
        dataset = new ArrayList<>();
        dataset.add("Item 1");
        dataset.add("Item 2");
        dataset.add("Item 3");
        dataset.add("Item 4");
        dataset.add("Item 5");
        dataset.add("Item 6");
        dataset.add("Item 7");
        dataset.add("Item 8");
        dataset.add("Item 9");
        dataset.add("Item 1");
        dataset.add("Item 2");
        dataset.add("Item 3");
        dataset.add("Item 4");
        dataset.add("Item 5");
    }

    private View.OnClickListener getItemViewOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomListViewHolder holder = (CustomListViewHolder) v.getTag();

                int adapterPosition = holder.getAdapterPosition();

                logger.info("Clicked: " + dataset.get(adapterPosition));
            }
        };
    }

    private SearchView.OnQueryTextListener getOnQueryTextListener() {
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                logger.info("Query text change: " + newText);
                return false;
            }
        };

    }

//    void initViews() {
//        button2 = findViewById(R.id.button2);
//    }

//    void initListeners() {
//        button2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                callImplicitActivity();
//            }
//        });
//    }

    void callImplicitCustomActivity() {
        Intent customActionIntent = new Intent();

        customActionIntent.setAction("ukim.finki.mpip2019.CUSTOM_ACTION");
        customActionIntent.addCategory("android.intent.category.DEFAULT");

        startActivity(customActionIntent);
    }

    void callImplicitActivity() {
        Intent implicitIntent = new Intent();

        implicitIntent.setAction(ACTION_SET_ALARM);
        implicitIntent.putExtra(EXTRA_HOUR, 6);
        implicitIntent.putExtra(EXTRA_MINUTES, 10);
        implicitIntent.setType("text/plain");
        implicitIntent.addCategory("android.intent.category.DEFAULT");

        Intent chooser = Intent.createChooser(implicitIntent, "Choose the desired app!");

        if(chooser.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser);
        }
    }
}
