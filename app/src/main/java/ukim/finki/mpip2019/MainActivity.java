package ukim.finki.mpip2019;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    private Logger logger = Logger.getLogger("MainActivity");

    private Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initListeners();

        logger.info("INFO: ON Create");
    }

    @Override
    protected void onStart() {
        super.onStart();

        logger.info("INFO: ON Start");
    }

    @Override
    protected void onResume() {
        super.onResume();

        logger.info("INFO: ON Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();

        logger.info("INFO: ON Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        logger.info("INFO: ON Stop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        logger.info("INFO: ON Destroy");
    }

    void initViews() {
        button1 = findViewById(R.id.button1);

    }

    void initListeners() {
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchActivity();
            }
        });
    }

    void launchActivity() {
        Intent intent = new Intent(this, Activity2.class);

        startActivity(intent);
    }


}
