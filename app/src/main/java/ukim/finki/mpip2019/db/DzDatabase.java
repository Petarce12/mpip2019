package ukim.finki.mpip2019.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import ukim.finki.mpip2019.models.DzTrack;

@Database(entities = {DzTrack.class}, version = 1)
public abstract class DzDatabase extends RoomDatabase {

    public abstract DzTrackDao getDzTrackDao();

}
