package ukim.finki.mpip2019.repository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import ukim.finki.mpip2019.models.DzPlaylist;

public interface DzPlaylistService {

    @GET("playlist/{id}")
    Call<DzPlaylist> getPlaylist(@Path("id") Long id);

}
